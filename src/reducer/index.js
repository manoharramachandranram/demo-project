import { combineReducers } from "redux";
import login from "./login_Reducer";

export default combineReducers({ login });
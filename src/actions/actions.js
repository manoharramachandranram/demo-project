import { LOGIN_VALIDATION, GET_USERLIST } from "./actionType";
export const get_loginvalues = () => {
    return {
        type: LOGIN_VALIDATION,
    }
};

export const get_uservalues = () => {
    return {
        type: GET_USERLIST,
    }
}
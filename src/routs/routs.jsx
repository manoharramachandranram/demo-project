import React, { Component } from 'react'
import { Route, Link, BrowserRouter as Router } from "react-router-dom";
import Login from '../Components/Login/Login';
import Users from '../Components/Users/User';
import history from '../History/history';

class Routes extends Component {
    state = {}
    render() {
        return (
            <Router history={history}>
                <div>
                    <Route path="/" exact component={Login} />
                    <Route path="/userlist" component={Users} />
                </div>
            </Router>
        );
    }
}

export default Routes;
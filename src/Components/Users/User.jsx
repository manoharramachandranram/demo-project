import React, { Component } from 'react';
import * as Actions from "../../actions/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DataTable from "react-data-table-component";
import './style.css';
class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    componentWillMount() {
        this.props.actions.get_uservalues();
    }
    render() {
        const columns = [
            {
                name: "ID",
                selector: "id",
                sortable: true,
                width: "70px",
                center: true,
            },
            {
                name: "Name",
                selector: "name",
                sortable: true,
                center: true,
            },
            {
                name: "Age",
                selector: "age",
                sortable: true,
                center: true,
            },
            {
                name: "Gender",
                selector: "gender",
                sortable: true,
                center: true,
            },
            {
                name: "Email",
                selector: "email",
                sortable: true,
                center: true,
            },
            {
                name: "PhoneNo",
                selector: "phoneNo",
                sortable: true,
                center: true,
            }
        ]
        console.log(this.props.userlist, "user")
        return (
            <div className="page-load">
                <h1>Welcome to usersList</h1>
                <DataTable
                    columns={columns}
                    data={this.props.userlist}
                />
            </div>
        );
    }
}
function mapStateToProps(state) {
    return { ...state, userlist: state.login.user };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Users)
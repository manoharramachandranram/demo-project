import React, { Component } from 'react';
import * as Actions from "../../actions/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import history from '../../History/history';

import './style.css';
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user_name: "",
            password: "",
            user_flag: false,
            password_flag: false
        }
    }
    componentWillMount() {
        this.props.actions.get_loginvalues();
    }
    handleChange(e) {
        this.setState({ user_name: { ...this.state.user_name, [e.target.name]: e.target.value }, password: { ...this.state.password, [e.target.name]: e.target.value } })
    }
    loginForm() {
        let login = this.props.loginvalue
        if (this.state.user_name.user_name !== "" || this.state.password.password !== "") {
            if (login.username != this.state.user_name.user_name) {
                this.setState({ user_flag: true, password_flag: false })
            } else if (login.password != this.state.password.password) {
                this.setState({ password_flag: true, user_flag: false })
            } else {
                this.setState({ user_flag: false, password_flag: false })
                history.push('/userlist');
                window.location.reload();
            }
        } else {
            this.setState({ user_flag: true, password_flag: true })
        }


    }
    render() {
        return (
            <div className="page-load">
                <h1>Login page</h1>
                <div className="login-form">
                    <div className="form-group">
                        <label htmlFor="">User name*</label>
                        <input type="text" className="form-control" name="user_name" onChange={this.handleChange.bind(this)} />
                        {this.state.user_flag ? <span className="error">User name is incorrect</span> : ""}
                    </div>
                    <div className="form-group">
                        <label htmlFor="">Password*</label>
                        <input type="text" className="form-control" name="password" onChange={this.handleChange.bind(this)} />
                        {this.state.password_flag ? <span className="error">Password is incorrect</span> : ""}
                    </div>
                    <div className="submit-btn">
                        <button className="btn btn-primary" onClick={this.loginForm.bind(this)}>Login</button>
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    return { ...state, loginvalue: state.login.login };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)